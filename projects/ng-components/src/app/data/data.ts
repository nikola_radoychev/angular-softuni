import { Article } from '../models/article.model';
import { data } from './seed';

export class ArticleData {
  getData(): Array<Article> {
    return data.map((article) => {
      const { title, description, author, imageUrl } = article;
      return new Article(title, description, author, imageUrl);
    });
  }
}
