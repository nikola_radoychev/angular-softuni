import { Article } from '../models/article.model';

const data: Array<Article> = [
  {
    title: 'Article 1',
    description:
      'Description Description Description Description Description Description Description Description Description Description Description ',
    author: 'Author 1',
    imageUrl:
      'https://images.unsplash.com/photo-1597679750942-9cf763e740c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80',
  },
  {
    title: 'Article 2',
    description:
      'Description Description Description Description Description Description Description Description Description Description Description ',
    author: 'Author 2',
    imageUrl:
      'https://images.unsplash.com/photo-1597679750942-9cf763e740c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80',
  },
  {
    title: 'Article 3',
    description:
      'Description Description Description Description Description Description Description Description Description Description Description ',
    author: 'Author 3',
    imageUrl:
      'https://images.unsplash.com/photo-1597679750942-9cf763e740c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80',
  },
  {
    title: 'Article 4',
    description:
      'Description Description Description Description Description Description Description Description Description Description Description ',
    author: 'Author 4',
    imageUrl:
      'https://images.unsplash.com/photo-1597679750942-9cf763e740c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80',
  },
  {
    title: 'Article 5',
    description:
      'Description Description Description Description Description Description Description Description Description Description Description ',
    author: 'Author 5',
    imageUrl:
      'https://images.unsplash.com/photo-1597679750942-9cf763e740c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80',
  },
];

export { data };
