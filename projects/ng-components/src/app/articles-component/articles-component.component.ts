import { Component, OnInit } from '@angular/core';

import { Article } from '../models/article.model';
import { ArticleData } from '../data/data';

@Component({
  selector: 'app-articles-component',
  templateUrl: './articles-component.component.html',
  styleUrls: ['./articles-component.component.scss'],
})
export class ArticlesComponentComponent implements OnInit {
  articles: Array<Article>;

  constructor() {}

  ngOnInit(): void {
    this.articles = new ArticleData().getData();
  }
}
